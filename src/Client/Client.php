<?php

namespace Hyve\AUTH\SDP\SDK\Client;

use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\SubscriptionService;
use Hyve\AUTH\SDP\SDK\Adapters\Http\HttpAdapter;

/**
 * Interface Subscription
 *
 * @package Hyvemobile\Vodacom\Vlive\SDK\Subscription
 */
interface Client
{
    /**
     * @return SubscriptionService
     */
    public function getSubscriptionService(): SubscriptionService;

    /**
     * @return \Hyve\AUTH\SDP\SDK\Client\HttpAdapter
     */
    public function getHttpAdapter(): HttpAdapter;

    /**
     * @return string
     */
    public function getAwsSecretKey(): string;

    /**
     * @return string
     */
    public function getAwsAccessKey(): string;

    /**
     * @return string
     */
    public function getSdpApiGatewayEndpoint(): string;

    /**
     * @return string
     */
    public function getSdpApiGatewayQuery(): string;

    /**
     * @return string
     */
    public function getSdpApiGatewayQueryValue(): string;

}