<?php

namespace Hyve\AUTH\SDP\SDK\Client;

use Hyve\AUTH\SDP\SDK\Adapters\Http\HttpAdapter;
use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\SubscriptionService;

class DefaultClient implements Client
{
    /**
     * @var
     */
    private $httpAdapter;

    /**
     * @var
     */
    private $subscriptionService;

    /**
     * @var
     */
    private $awsSecretKey;

    /**
     * @var
     */
    private $awsAccessKey;

    /**
     * @var
     */
    private $sdpApiGwEndpoint;

    /**
     * @var
     */
    private $sdpApiGwEndpointQuery;

    /**
     * @var
     */
    private $sdpApiGwEndpointQueryValue;

    /**
     * DefaultClient constructor.
     *
     * @param array $args
     */
    public function __construct(array $args)
    {
        foreach (array_merge($args['config'], $args['libraries']) as $key => $val) {
            $this->{$key} = $val;
        }
    }

    /**
     * @return \Hyve\AUTH\SDP\SDK\Client\Sdp\UserSubscription\SubscriptionService
     */
    public function getSubscriptionService(): SubscriptionService
    {
        return $this->subscriptionService->setClient($this);
    }

    /**
     * @return \Hyve\AUTH\SDP\SDK\Adapters\Http\HttpAdapter
     */
    public function getHttpAdapter(): HttpAdapter
    {
        return $this->httpAdapter;
    }

    /**
     * @return string
     */
    public function getAwsSecretKey(): string
    {
        return $this->awsSecretKey;
    }

    /**
     * @return string
     */
    public function getAwsAccessKey(): string
    {
        return $this->awsAccessKey;
    }

    /**
     * @return string
     */
    public function getSdpApiGatewayEndpoint(): string
    {
        return $this->sdpApiGwEndpoint;
    }

    /**
     * @return string
     */
    public function getSdpApiGatewayQuery(): string
    {
        return $this->sdpApiGwEndpointQuery;
    }

    /**
     * @return string
     */
    public function getSdpApiGatewayQueryValue(): string
    {
        return $this->sdpApiGwEndpointQueryValue;
    }

    /**
     * @return string
     */
    public function awsCredentials(): string
    {
        return $this->awsCredentials;
    }

    /**
     * @return string
     */
    public function awsSignatureV4(): string
    {
        return $this->awsSignatureV4;
    }
}
