<?php

namespace Hyve\AUTH\SDP\SDK\Client\SubscriptionService;

use Hyve\AUTH\SDP\SDK\Client\Client;
use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\SubscriptionService;
use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\ValueObjects\UserSubscriptionByIdResponse;
use Hyve\AUTH\SDP\SDK\Adapters\Http;

/**
 * Class DefaultSubscriptionService
 *
 * @package Hyve\AUTH\SDP\SDK\Client\Sdp\UserSubscription
 */
class DefaultSubscriptionService implements SubscriptionService
{
    /**
     * @var
     */
    protected $client;

    /**
     * @var
     */
    protected $configuredEndpoint;

    /**
     * @param \Hyve\AUTH\SDP\SDK\Client\Client $client
     *
     * @return \Hyve\AUTH\SDP\SDK\Client\Sdp\UserSubscription\BillingService
     */
    public function setClient(Client $client): SubscriptionService
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @param int $subscriptionId
     *
     * @return array
     */
    public function userSubscriptionByIdRequest(): UserSubscriptionByIdResponse
    {
        $this->configuredEndpoint = $this->client->getSdpApiGatewayEndpoint() . '/' . $this->client->getSdpApiGatewayQuery() . '/' . $this->client->getSdpApiGatewayQueryValue();
        $subscriptionResponse = $this->client->getHttpAdapter()->get($this->configuredEndpoint);

        return new UserSubscriptionByIdResponse($subscriptionResponse);
    }
}