<?php

namespace Hyve\AUTH\SDP\SDK\Client\SubscriptionService\ValueObjects;

class UserSubscriptionByIdResponse
{
    /**
     * @var mixed
     */
    private $response;

    /**
     * UserSubscriptionResponse constructor.
     *
     * @param \stdClass $response
     */
    public function __construct(\stdClass $response)
    {
        $this->response = $response;
    }

    /**
     * @return int
     */
    public function getSubscriptionId(): int
    {
        return $this->response->subscription_id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->response->user_id;
    }

    /**
     * @return string
     */
    public function getUserMsisdn(): string
    {
        return $this->response->user_msisdn;
    }

    /**
     * @return int
     */
    public function getSvcId(): int
    {
        return $this->response->svc_id;
    }

    /**
     * @return string
     */
    public function getChannelName(): string
    {
        return $this->response->wap;
    }

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return $this->response->status_id;
    }

    /**
     * @return int
     */
    public function getStatusName(): int
    {
        return $this->response->status_name;
    }

    /**
     * @return string
     */
    public function getRenewalType(): string
    {
        return $this->response->renewal_type;
    }

    /**
     * @return int
     */
    public function getAffiliateId(): int
    {
        return $this->response->affiliate_id;
    }

    /**
     * @return int
     */
    public function getBillingRateId(): int
    {
        return $this->response->billing_rate_id;
    }

    /**
     * @return string
     */
    public function getBillingCycle(): string
    {
        return $this->response->billing_cycle;
    }

    /**
     * @return int
     */
    public function getCreatedAtTime(): int
    {
        return $this->response->created_at;
    }

    /**
     * @return string
     */
    public function getSubscriptionStartedAtTime(): string
    {
        return $this->response->subscription_started_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAtTime(): string
    {
        return $this->response->updated_at;
    }

    /**
     * @return string
     */
    public function getExpiresAtTime(): string
    {
        return $this->response->updated_at;
    }

    /**
     * @return string
     */
    public function getLastBilledAt(): string
    {
        return $this->response->last_billed_at;
    }

    /**
     * @return string
     */
    public function getNextBillingAt(): string
    {
        return $this->response->next_billing_at;
    }
}
