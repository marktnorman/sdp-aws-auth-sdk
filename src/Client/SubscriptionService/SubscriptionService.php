<?php

namespace Hyve\AUTH\SDP\SDK\Client\SubscriptionService;

use Hyve\AUTH\SDP\SDK\Client\Client;
use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\ValueObjects\UserSubscriptionByIdResponse;

interface SubscriptionService
{
    /**
     * @param \Hyve\MTC\IN\SDK\Client\Client $client
     *
     * @return \Hyve\AUTH\SDP\SDK\Client\Sdp\UserSubscription\SubscriptionService
     */
    public function setClient(Client $client): SubscriptionService;

    /**
     * @return \Hyve\AUTH\SDP\SDK\Client\Sdp\UserSubscription\SubscriptionService
     */
    public function userSubscriptionByIdRequest(): UserSubscriptionByIdResponse;
}