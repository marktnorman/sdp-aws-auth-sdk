<?php

namespace Hyve\AUTH\SDP\SDK\Adapters\Http;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request;
use Aws\Credentials\Credentials;
use Aws\Signature\SignatureV4;

class DefaultHttpAdapter extends GuzzleClient implements HttpAdapter
{
    /**
     * @param \Psr\Http\Message\UriInterface|string $endpoint
     * @param array                                 $payload
     * @param array                                 $headers
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function post($endpoint)
    {
        $gz = new GuzzleClient();
        $req = new Request('POST', $endpoint . '?address=2&limit=1');
        $req = $this->auth($req);
        $req = $req->withAddedHeader('Content-Type', 'application/json');
        $req = $req->withAddedHeader('Accept', 'application/vnd.hyvesdp.com+json;version=1.*');
        $res = $gz->send($req);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * @param \Psr\Http\Message\UriInterface|string $endpoint
     * @param array                                 $headers
     * @param array                                 $urlParams
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function get($endpoint)
    {
        $gz = new GuzzleClient();
        $req = new Request('GET', $endpoint . '?address=2&limit=1');
        $req = $this->auth($req);
        $req = $req->withAddedHeader('Content-Type', 'application/json');
        $req = $req->withAddedHeader('Accept', 'application/vnd.hyvesdp.com+json;version=1.*');
        $res = $gz->send($req);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * @param $req
     *
     * @return \GuzzleHttp\Psr7\Request|\Psr\Http\Message\RequestInterface
     */
    private function auth($req) {
        $credentials = new Credentials(env('AWS_ACCESS_KEY'), env('AWS_SECRET_KEY'));
        $sig = new SignatureV4('execute-api', 'eu-west-1');
        $req = $sig->signRequest($req, $credentials);

        return $req;
    }
}