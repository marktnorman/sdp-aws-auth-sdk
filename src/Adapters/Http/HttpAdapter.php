<?php

namespace Hyve\AUTH\SDP\SDK\Adapters\Http;

interface HttpAdapter
{
    /**
     * @param $endpoint
     *
     * @return mixed
     */
    public function post($endpoint);

    /**
     * @param $endpoint
     *
     * @return mixed
     */
    public function get($endpoint);

    /**
     * @param $endpoint
     *
     * @return mixed
     */
    public function auth($endpoint);
}