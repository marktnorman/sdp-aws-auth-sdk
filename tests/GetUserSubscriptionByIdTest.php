<?php

namespace Hyvemobile\Vodacom\Vlive\Tests\Mocks\Services;

use Hyve\AUTH\SDP\SDK\Client\DefaultClient;
use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\UserSubscription;
use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\ValueObjects\UserSubscriptionByIdResponse;
use Hyve\AUTH\SDP\SDK\Adapters\Http\DefaultHttpAdapter;
use Hyve\AUTH\SDP\SDK\Tests\Mocks\Services\MockSubscriptionService;
use Aws\Credentials\Credentials;
use Aws\Signature\SignatureV4;
use PHPUnit\Framework\TestCase;

/**
 * Class MockSubscriptionService
 *
 * @package Hyvemobile\Vodacom\Vlive\SDK\Tests\Mocks\Services
 */
class GetUserSubscriptionByIdTest extends TestCase
{
    /**
     * @var DefaultClient
     */
    private $client;

    public function __construct()
    {
        $this->client = new DefaultClient([
            'config' => [
                'subscription_id' => 223427,
                'aws_secret_key' => '',
                'aws_access_key' => '',
                'sdp_api_gw_endpoint' => '',
                'sdp_api_gw_query' => '',
                'sdp_api_gw_query_value' => '',
            ],
            'libraries' => [
                'subscriptionService' => new MockSubscriptionService(),
                'httpAdapter' => new DefaultHttpAdapter(),
                'awsCredentials' => new Credentials(env('AWS_ACCESS_KEY'), env('AWS_SECRET_KEY')),
                'awsSignatureV4' => new SignatureV4('execute-api', 'eu-west-1')
            ]
        ]);

        parent::__construct();
    }

    public function testGetUserSubscriptionByIdTest()
    {
        $res = $this->client->getSubscriptionService()->userSubscriptionByIdRequest();
        $this->assertInstanceOf(UserSubscriptionByIdResponse::class, $res, 'Given value is not an instance of expected.');

        return $res;
    }
}