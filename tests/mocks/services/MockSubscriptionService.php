<?php

namespace Hyve\AUTH\SDP\SDK\Tests\Mocks\Services;

use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\ValueObjects\UserSubscriptionByIdResponse;
use Hyve\AUTH\SDP\SDK\Client\SubscriptionService\DefaultSubscriptionService;

/**
 * Class MockSubscriptionService
 *
 * @package Hyve\AUTH\SDP\SDK\Tests\Mocks\Services
 */
class MockSubscriptionService extends DefaultSubscriptionService
{
    public function userSubscriptionByIdRequest(): UserSubscriptionByIdResponse
    {
        $res = json_decode(file_get_contents(__DIR__ . '/../../Responses/UserSubscription/Success.json'));

        return new UserSubscriptionByIdResponse($res);
    }

}