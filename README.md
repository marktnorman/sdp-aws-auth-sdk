# SDP AUTH SDK

This Composer package supplies the user with AUTH enable requests to create / retrieve subscriptions and monitor user information

## Installation of SDP AUTH SDK

```php
{"type":"git","url": "https://bitbucket.org/onnet/laravel.mcm.sdk.git"}

"hyve/mcm/sdk": "dev-develop"
```

## AWS SDK PHP Repo (Installation)

```
$ composer require aws/aws-sdk-php-resources
```

## MCM SDK connecting to the SDP
Sole purpose of this SDK is to connect to the SDP, retrieving user information, subscription information as well as create subscriptions.

## Official SDP API documentation (SWAGGER)
Here you'll find all the required information needed to make successful requests -
http://docs.api-gw.hyvesdp.com

## Official AWS SDK PHP documentation
https://aws.amazon.com/sdk-for-php/

## Official AWS SDK PHP repo
https://github.com/awslabs/aws-sdk-php-resources

##AWS SDK PHP Packagist
https://packagist.org/packages/aws/aws-sdk-php-resources

## Instantiating client example

```php
new DefaultClient([
            'config' => [
                'awsSecretKey' => env('AWS_SECRET_KEY'),
                'awsAccessKey' => env('AWS_ACCESS_KEY'),
                'sdpApiGwEndpoint' => env('SDP_API_GW'),
                'sdpApiGwEndpointQuery' => 'msisdn',
                'sdpApiGwEndpointQueryValue' => '27641098637',
            ],
            'libraries' => [
                'subscriptionService' => new DefaultSubscriptionService(),
                'httpAdapter' => new DefaultHttpAdapter()
            ]
]);
```

## Authentication
Signature V4 Requirements -

AWS access key (AWS_ACCESS_KEY)
AWS secret key (AWS_SECRET_KEY)

Those details are stored in environment files with the above names in brackets.

## Signature SDK example

```php
/**
     * @param \Psr\Http\Message\UriInterface|string $endpoint
     * @param array                                 $headers
     * @param array                                 $urlParams
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function get($endpoint)
    {
        $gz = new GuzzleClient();
        $req = new Request('GET', $endpoint . '?address=2&limit=1');
        $req = $this->auth($req);
        $req = $req->withAddedHeader('Content-Type', 'application/json');
        $req = $req->withAddedHeader('Accept', 'application/vnd.hyvesdp.com+json;version=1.*');
        $res = $gz->send($req);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * @param $req
     *
     * @return \GuzzleHttp\Psr7\Request|\Psr\Http\Message\RequestInterface
     */
    private function auth($req) {
        $credentials = new Credentials(env('AWS_ACCESS_KEY'), env('AWS_SECRET_KEY'));
        $sig = new SignatureV4('execute-api', 'eu-west-1');
        $req = $sig->signRequest($req, $credentials);

        return $req;
    }
```

### Get user subscription by id example

After you've instantiated the client, we now can start using the client in order to retrieve what we require, all methods below -

```php
public function userSubscriptionByIdRequest(): UserSubscriptionByIdResponse
{
    $this->configuredEndpoint = $this->client->getSdpApiGatewayEndpoint() . '/' . $this->client->getSdpApiGatewayQuery() . '/' . $this->client->getSdpApiGatewayQueryValue();
    $subscriptionResponse = $this->client->getHttpAdapter()->get($this->configuredEndpoint);

    return new UserSubscriptionByIdResponse($subscriptionResponse);
}
```

### Value Objects

- UserSubscriptionByIdResponse

### HTTP Adapter

The adapter's only responsibility is to supply us with GET + POST requests while signing the requests by using the SignatureV4 library and Credentials library.

Below example of POST + GET + AUTH functions

```php
/**
     * @param \Psr\Http\Message\UriInterface|string $endpoint
     * @param array                                 $payload
     * @param array                                 $headers
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function post($endpoint)
    {
        $gz = new GuzzleClient();
        $req = new Request('POST', $endpoint . '?address=2&limit=1');
        $req = $this->auth($req);
        $req = $req->withAddedHeader('Content-Type', 'application/json');
        $req = $req->withAddedHeader('Accept', 'application/vnd.hyvesdp.com+json;version=1.*');
        $res = $gz->send($req);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * @param \Psr\Http\Message\UriInterface|string $endpoint
     * @param array                                 $headers
     * @param array                                 $urlParams
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function get($endpoint)
    {
        $gz = new GuzzleClient();
        $req = new Request('GET', $endpoint . '?address=2&limit=1');
        $req = $this->auth($req);
        $req = $req->withAddedHeader('Content-Type', 'application/json');
        $req = $req->withAddedHeader('Accept', 'application/vnd.hyvesdp.com+json;version=1.*');
        $res = $gz->send($req);

        return json_decode($res->getBody()->getContents());
    }

    /**
     * @param $req
     *
     * @return \GuzzleHttp\Psr7\Request|\Psr\Http\Message\RequestInterface
     */
    private function auth($req) {
        $credentials = new Credentials(env('AWS_ACCESS_KEY'), env('AWS_SECRET_KEY'));
        $sig = new SignatureV4('execute-api', 'eu-west-1');
        $req = $sig->signRequest($req, $credentials);

        return $req;
    }
```

## Developers 
- Mark Norman <mark@hyvemobile.co.za>